#include "Window.h"
 
void vk::Window::BorderDraw(){
	/*CORNERS*/
	//left-up
	Console.SetCursorPositon(x, y);
	Console.Write(CC(201));
	//right-up
	Console.SetCursorPositon(CS(x + w - 1), y);
	Console.Write(CC(187));
	//right-down
	Console.SetCursorPositon(x,CS( y + h - 2));
	Console.Write(CC(200));
	//left-down
	Console.SetCursorPositon(CS(x + w - 1), CS(y + h - 2));
	Console.Write(CC(188));

	//sides
	for (int i = 1; i < w - 1; i++) {
		Console.SetCursorPositon(CS(x + i), y);
		Console.Write(CC(205));
		Console.SetCursorPositon(CS(x + i), CS(y + h-2));
		Console.Write(CC(205));
	}
	for (int i = 1; i < h - 2; i++) {
		Console.SetCursorPositon(x, CS(y + i));
		Console.Write(CC(186));
		Console.SetCursorPositon(CS(x + w - 1), CS(y + i));
		Console.Write(CC(186));
	}
	
}

vk::Window::Window(
	int x, int y,
	int w, int h,
	_Console::ConsoleColor bg,
	_Console::ConsoleColor border_c
) :
	bg(bg), border_c(border_c)
{
	if (x < 0)x = 0;
	if (y < 0)y = 0;
	if (w < 3)w = 3;
	if (h < 3)h = 3;
	this->x = CS(x);
	this->y = CS(y);
	this->w = CS(w);
	this->h = CS(h);
}

void vk::Window::Show(){	
	//background
	Console.SetCursorPositon(x, y);
	Console.SetColor(bg, border_c);
	for (int i = 0; i < h; i++)
	{
		for (int j = 0; j < w; j++)
			Console.Write(" ");
		Console.SetCursorPositon(x, CS(y + i));
	}
	BorderDraw();
	Draw();
}

void vk::Window::Draw(){}







/*DESKTOP*/
vk::Desktop::Desktop(int h, vk::_Console::ConsoleColor bg,
	vk::_Console::ConsoleColor border):
	vk::Window(0,0,vk::Console.GetScreenBufferSize().x,h,bg,border){

}

vk::Message::Message(const char *msg, int x, int y, int w, int h, 
	_Console::ConsoleColor bg,
	_Console::ConsoleColor border,
	_Console::ConsoleColor fg):
	Window(x,y,w,h,bg,border), bt_ok(x,y,bg,fg,border)
{
	txt_len = (int)(1 + strlen(msg));
	if (txt_len >= w - 2) 
		txt_len = w - 2;
	this->msg = new char[txt_len];
	strcpy_s(this->msg, txt_len, msg);
}

void vk::Message::Draw(){
	short d_y = CS(y + 1 + h / 2 - 2);
	short d_x = CS(x + 1 + w / 2 - txt_len / 2-1);
	Console.SetCursorPositon(d_x, d_y);
	Console.Write(msg);
}

vk::Message::~Message(){
	delete msg;
}
