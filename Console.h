#pragma once
#include "Vector2.h"
#include <stdio.h>
#include <conio.h>
#include <Windows.h>
#include "Resourses.h"


#pragma pack(push, VK_PACK)
#pragma warning (disable: 4996)
#ifndef VK_NAMESPACE

#define VK_NAMESPACE
#define VK_NAMESPACE_BEG namespace vk{
#define VK_NAMESPACE_END }

#endif

VK_NAMESPACE_BEG
class _Console{
public:
	/*READ*/
	_Console&	Read				(int &data) {
		scanf_s("%d", &data);
		return *this;
	}
	_Console&	Read				(unsigned &data) {
		scanf_s("%u", &data);
		return *this;
	}
	_Console&	Read				(float &data) {
		scanf_s("%f", &data);
		return *this;
	}
	_Console&	Read				(double &data) {
		scanf_s("%lf", &data);
		return *this;
	}
	_Console&	Read				(char *data, int len) {
		gets_s(data, len);
		return *this;
	}
	/*READ LINE*/
	template<class T>
	_Console&	ReadLine			(T &data) {
		scanf_s("%d", &data);
		printf_s("\n");
		return *this;
	}
	_Console&	ReadLine			(unsigned &data) {
		scanf_s("%u", &data);
		printf_s("\n");
		return *this;
	}
	_Console&	ReadLine			(float &data) {
		scanf_s("%f", &data);
		printf_s("\n");
		return *this;
	}
	_Console&	ReadLine			(double &data) {
		scanf_s("%lf", &data);
		printf_s("\n");
		return *this;
	}
	/*READ KEY*/
	char		ReadKey				() {
		return CC(_getch());
	}
	_Console&	ReadKey				(char &key) {
		key = CC(_getch());
		return *this;
	}



	/*WRITE*/
	_Console&	Write				(int data) {
		printf_s("%d", data);
		//d - integer
		//f - float
		//lf - double
		//c - char
		//s - string
		return *this;
	}
	_Console&	Write				(unsigned data) {
		printf_s("%u", data);
		return *this;
	}
	_Console&	Write				(float data) {
		printf_s("%f", data);
		return *this;
	}
	_Console&	Write				(double data) {
		printf_s("%lf", data);
		return *this;
	}
	_Console&	Write				(const char * data) {
		printf_s("%s", data);
		return *this;
	}
	_Console&	Write				(char data) {
		printf_s("%c", data);
		return *this;
	}
	_Console&	Write				(char *data) {
		printf_s("%s", data);
		return *this;
	}
	_Console&	Write				(unsigned char data) {
		printf_s("%c", data);
		return *this;
	}
	_Console&	Write				() {
		printf_s(" ");
		return *this;
	}

	/*WRITE LINE*/
	_Console&	WriteLine			(int data) {// T = int
		printf_s("%d", data);
		printf_s("\n");
		return *this;
	}
	_Console&	WriteLine			(unsigned data) {
		printf_s("%u", data);
		printf_s("\n");
		return *this;
	}
	_Console&	WriteLine			() {
		printf_s("\n");
		return *this;
	}
	_Console&	WriteLine			(float data) {
		printf_s("%f", data);
		printf_s("\n");
		return *this;
	}
	_Console&	WriteLine			(double data) {
		printf_s("%lf", data);
		printf_s("\n");
		return *this;
	}
	_Console&	WriteLine			(const char * data) {
		printf_s("%s", data);
		printf_s("\n");
		return *this;
	}
	_Console&	WriteLine			(char data) {
		printf_s("%c", data);
		printf_s("\n");
		return *this;
	}
	_Console&	WriteLine			(char *data) {
		printf_s("%s", data);
		printf_s("\n");
		return *this;
	}
	_Console&	WriteLine			(unsigned char data) {
		printf_s("%c", data);
		printf_s("\n");
		return *this;
	}
	_Console&	WriteLine			(long long *data) {
		printf_s("%p", data);
		printf_s("\n");
		return *this;
	}



	/*WORK WITH THE STYLE*/
	enum class ConsoleColor {
		Black,
		Blue,
		Green,
		Aqua,
		Red,
		Purple,
		Yellow,
		White,
		Gray,
		LightBlue,
		LightGreen,
		LightAqua,
		LightRed,
		LightPurple,
		LightYellow,
		BrightWhite
	};
	_Console&	SetColor			(ConsoleColor bg, ConsoleColor fg) {
		/*
		bit 0 - foreground blue
		bit 1 - foreground green
		bit 2 - foreground red
		bit 3 - foreground intensity
		
		bit 4 - background blue
		bit 5 - background green
		bit 6 - background red
		bit 7 - background intensity
		*/
		SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE),
			static_cast<WORD>(fg) | (static_cast<WORD>(bg) << 4));
		return *this;
	}
	_Console&	SetCursorPositon	(SHORT x, SHORT y) {		
		SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), { x,y });
		return *this;
	}
	_Console&	Clear				() {
		system("cls");
		return *this;
	}
	_Console&	Clear				(int len){
		for (int i = 0; i < len; i++)
			this->Write(" ");
		return *this;
	}
	_Console&	Clear				(int len ,SHORT x, SHORT y) {
		SetCursorPositon(x, y);
		for (int i = 0; i < len; i++)
			this->Write(" ");
		return *this;
	}
	_Console&	Clear				(int len, SHORT x, SHORT y, 
										ConsoleColor bg, ConsoleColor fg) {
		SetColor(bg, fg);
		SetCursorPositon(x, y);
		for (int i = 0; i < len; i++)
			this->Write(" ");
		return *this;
	}
	_Console&	SetBufferSize		(SHORT x, SHORT y) {
		SetConsoleScreenBufferSize(GetStdHandle(STD_OUTPUT_HANDLE), {x,y});
		return *this;

	}
	SHORT		GetRows				()	{
		CONSOLE_SCREEN_BUFFER_INFO c;
		if (!GetConsoleScreenBufferInfo(GetStdHandle(STD_OUTPUT_HANDLE), &c))
			return 0;
		return c.dwSize.Y;
	}
	SHORT		GetCols				(){
		CONSOLE_SCREEN_BUFFER_INFO c;
		if (!GetConsoleScreenBufferInfo(GetStdHandle(STD_OUTPUT_HANDLE), &c))
			return 0;
		return c.dwSize.X;
	}
	SHORT		GetCurrentY			(){
		CONSOLE_SCREEN_BUFFER_INFO c;
		if (!GetConsoleScreenBufferInfo(GetStdHandle(STD_OUTPUT_HANDLE), &c))
			return 0;
		return c.dwCursorPosition.Y;
	}
	SHORT		GetCurrentX			(){
		CONSOLE_SCREEN_BUFFER_INFO c;
		if (!GetConsoleScreenBufferInfo(GetStdHandle(STD_OUTPUT_HANDLE), &c))
			return 0;
		return c.dwCursorPosition.X;
	}
	_Console&	IsShowCaret			(BOOL show)	{
		CONSOLE_CURSOR_INFO ci;
		ci.bVisible = show;
		SetConsoleCursorInfo(GetStdHandle(STD_OUTPUT_HANDLE), &ci);
	}
	_Console&	SetCaretSize		(unsigned char size) {
		CONSOLE_CURSOR_INFO ci;
		ci.dwSize = size;
		SetConsoleCursorInfo(GetStdHandle(STD_OUTPUT_HANDLE), &ci);
		return *this;
	}
	_Console&	ResetColor			() {
		SetColor(ConsoleColor::Black, ConsoleColor::BrightWhite);
		return *this;
	}
	_Console&	SetTitle			(const char * title) {
		SetConsoleTitle(title);
		return *this;
	}
	_Console&	GetWndSize			(int &w, int &h){
		HWND hWnd = GetConsoleWindow();
		RECT rc;
		GetClientRect(hWnd, &rc);
		w = rc.right; // ������ ������� �������
		h = rc.bottom;
	}
	Vector2<int> GetScreenBufferSize() {
		CONSOLE_SCREEN_BUFFER_INFO c;
		GetConsoleScreenBufferInfo(
			GetStdHandle(STD_OUTPUT_HANDLE), &c);
		return Vector2<int>(c.dwSize.X, c.dwSize.Y);
	}



	/*GENERAL*/
	_Console&	operator=			(const _Console&)	= delete;
	_Console&	operator=			(const _Console&&)	= delete;
				_Console			()					= default;
				_Console			(const _Console&)	= delete;
				_Console			(const _Console&&)	= delete;
				~_Console			()					= default;
};
static _Console Console;
VK_NAMESPACE_END
#pragma warning (default: 4996)
#pragma pack(pop)