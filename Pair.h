#pragma once
#ifndef VK_NAMESPACE

#define VK_NAMESPACE
#define VK_NAMESPACE_BEG namespace vk{
#define VK_NAMESPACE_END }

#endif

VK_NAMESPACE_BEG
template<class _Tx, class _Ty>
class Pair {
public:
	_Tx x;
	_Ty y;
	~Pair() {
		int y;
	}
	Pair() = default;
	Pair(_Tx x, _Ty y) :x(x), y(y) {}
};		 
VK_NAMESPACE_END