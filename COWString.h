#pragma once
#include "Resourses.h"
#include "Exception.h"
#include "CString.h"
#ifndef VK_NAMESPACE
#define VK_NAMESPACE
#define VK_NAMESPACE_BEG namespace vk{
#define VK_NAMESPACE_END }
#endif 

VK_NAMESPACE_BEG
class COWString {
public:
	using type = char;
					COWString		(const type *s = 0);
					COWString		(COWString &s);
					~COWString		();
	COWString&		operator=		(COWString& s);
	COWString &		Copy			(const type * s, uint pos = 0, uint amo = 0);
	COWString &		Cat				(const type *s, uint amo = 0);
	COWString &		Cat				(const COWString & s, uint amo = 0);
	COWString &		Cat				(const type ch);
	uint			Length			()					const;
	uint			Capacity		();
	COWString &		Clear			();
	const type *	c_str			()					const;
	bool			IsEmpty			()					const;
	COWString&		Erase			(uint pos, uint amo);
	COWString		SubStr			(uint pos, uint amo)const;
	int				Find			(const type *s)		const;
	int				FindFirstOf		(char t)			const;
	int				FindLastOf		(char t)			const;
	int				FindFirstNotOf	(char t)			const;
	int				FindLastNotOf	(char t)			const;
	COWString &		Insert			(uint pos, const type *s);
	short			Compare			(const COWString &s)const;
	short			Compare			(const type *s)		const;
	//Coping old to new arr(all data are save), and expanding to new capacity/size
	COWString &		Resize			(uint sz);
	//Deleting all data, and expanding to new capacity/size
	COWString &		Reserve			(uint new_cap);
	type &			operator []		(uint i);
	COWString &		operator =		(const type *s);
	COWString &		operator +=		(const type *s);
	COWString &		operator +=		(const COWString &s);
	COWString &		operator +=		(const type s);
	COWString		operator +		(const type *s)		const;
	COWString		operator +		(const COWString &s)const;
	bool			operator >		(const type *s)		const;
	bool			operator >		(const COWString &s)const;
	bool			operator <		(const type *s)		const;
	bool			operator <		(const COWString &s)const;
	bool			operator >=		(const type *s)		const;
	bool			operator >=		(const COWString &s)const;
	bool			operator <=		(const type *s)		const;
	bool			operator <=		(const COWString &s)const;
	bool			operator ==		(const type *s)		const;
	bool			operator ==		(const COWString &s)const;
	bool			operator !=		(const type *s)		const;
	bool			operator !=		(const COWString &s)const;
	bool			operator !		();
	operator const type *()const;
protected:
	void			Clone();
	bool			is_copy;
	type			*str;
	uint			size;
	uint			capacity;
};
VK_NAMESPACE_END