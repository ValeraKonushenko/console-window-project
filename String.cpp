#include "String.h"

#pragma warning (disable: 4996)
VK_NAMESPACE_BEG
using type =	char;
				String::String		(const type *s){
	size = capacity = 0;
	str = nullptr;
	Copy(s);
}
				String::String		(String & s){
	size = capacity = 0;
	str = nullptr;
	Copy(s.c_str());
}
				String::~String		(){
	Clear();
}
uint			String::Length		()const{
	return size;
}
uint			String::Capacity	() {
	return capacity;
}
String &		String::Clear		(){
	delete[] this->str;
	size = capacity = 0;
	str = nullptr;
	return *this;
}
String &		String::Copy		(const type * s, uint pos, uint amo){
	if (s == 0 || s == nullptr)	return *this;

	uint new_len = static_cast<uint>(StrLen(s)) + 1;
	if (pos + new_len > capacity)		Resize(pos + new_len);

	if (amo == 0)amo -= 1;

	uint i;
	for (i = 0; s[i] && i < amo; i++)
		str[i + pos] = s[i];
	str[i + pos] = 0;
	size = pos + new_len - 1;
	return *this;
}
String &		String::Copy		(const String & s, uint pos, uint amo) {
	if (s.str == 0 || s.str == nullptr)	return *this;

	uint new_len = (amo == 0) ? s.Length() + 1 : amo + 1;
	if (pos + new_len > capacity)		Resize(pos + new_len);

	if (amo == 0)amo -= 1;

	uint i;
	for (i = 0; s.str[i] && i < amo; i++)
		str[i + pos] = s.str[i];
	str[i + pos] = 0;
	size = pos + new_len - 1;
	return *this;
}
String &		String::Cat			(const type * s, uint amo){
	if (s == 0 || s == nullptr)	return *this;

	uint new_len = (amo == 0)?StrLen(s) + 1: amo + 1;
	if (size + new_len > capacity)		Resize(size + new_len);

	if (amo == 0)amo -= 1;
	uint i = 0;
	for (i = 0; i < new_len && i < amo; i++)
		str[i + size] = s[i];
	size += new_len - 1;
	return *this;
}
String &		String::Cat			(const String & s, uint amo) {
	if (s.str == 0 || s.str == nullptr)	return *this;

	uint new_len = (amo == 0) ? s.Length() + 1 : amo + 1;
	if (size + new_len > capacity)		Resize(size + new_len);

	if (amo == 0)amo -= 1;
	uint i = 0;
	for (i = 0; i < new_len && i < amo; i++)
		str[i + size] = s.str[i];

	size += new_len - 1;
	return *this;
}
String &		String::Cat			(const type ch){
	if (size + 1 >= capacity)Resize(size + 1);
	str[size++] = ch;
	return *this;
}
const type *	String::c_str		(){
	return str;
}
bool			String::IsEmpty		(){
	return !size;
}
String &		String::Erase		(uint pos, uint amo){
	if (pos + amo > size)throw Exception("Violation of right acces to the memory");
	for (uint i = pos + amo; i <= size; i++) {
		str[i - amo] = str[i];
		if (!str[i])size = i - amo;
	}
	return *this;
}
String			String::SubStr		(uint pos, uint amo){
	if (pos + amo >= capacity)throw Exception("Vioaltion of right access to the memory");
	return String().Copy(str+pos,0,amo);
}
int				String::Find		(const type * s)const{
	uint new_size = StrLen(s);
	uint pos = 0;
	int len_counter = 0;
	for (uint i = 0; i < size && len_counter != new_size; i++)
		for (uint j = 0; j < new_size; j++)
			if (str[i + j] == s[j]) {
				pos = i + j;
				len_counter++;
			}
			else {
				len_counter = 0;
				break;
			}

	if (len_counter != 0)
		return pos + 1 - new_size;
	return -1;
}
String &		String::Insert		(uint pos, const type * s){
	uint len = StrLen(s);
	capacity += len;
	type *new_str = new type[capacity];
	uint i = 0, f = 0;
	//copying all before POS
	while (i < pos)
		new_str[i++] = str[f++];

	//copying the insert part
	for (uint l = 0; l < len; l++)
		new_str[i++] = s[l];

	//copying all after pos+len_it
	while(str[f])
		new_str[i++] = str[f++];
	new_str[i] = 0;
	size += len;
	delete[] str;
	str = new_str;
	return *this;
}
short			String::Compare		(String & s){
	return static_cast<type>(StrCmp(str, s.c_str()));
}
short			String::Compare		(const type * s){
	return static_cast<type>(StrCmp(str, s));
}
String &		String::Resize		(uint sz){
	type *new_str = new type[sz];
	for (uint i = 0; i <= size && str; i++)
		new_str[i] = str[i];
	delete[] str;
	str = new_str;
	capacity = sz;
	return *this;
}
String &		String::Reserve		(uint new_cap){
	type *new_str = new type[new_cap];
	Clear();
	str = new_str;
	capacity = new_cap;
	return *this;
}
type &			String::operator[](uint i){
	
	if (i >= capacity) throw Exception("Violation of right access to the memory");
	return str[i];
}
String &		String::operator=	(const type * s){
	Clear();
	Copy(s);
	return *this;
}
String &		String::operator=	(String &s) {
	Clear();
	Copy(s.c_str());
	return *this;
}
String &		String::operator+=	(const type * s){
	Cat(s);
	return *this;
}
String &		String::operator+=	(String & s){
	Cat(s);
	return *this;
}
String &		String::operator+=	(type s){
	Cat(s);
	return *this;
}
String			String::operator+	(const type * s){
	return String(str).Cat(s);
}
String			String::operator+	(String & s){
	return String(str).Cat(s);
}
bool			String::operator >	(const type *s) {
	if (Compare(s) == 1) return true;
	return false;
}
bool			String::operator >	(String &s) {
	if (Compare(s.str) == 1) return true;
	return false;
}
bool			String::operator <	(const type *s) {
	if (Compare(s) == -1) return true;
	return false;
}
bool			String::operator <	(String &s) {
	if (Compare(s.str) == -1) return true;
	return false;
}
bool			String::operator >=	(const type *s) {
	short answ = Compare(s);
	if (answ == 1 || answ == 0) return true;
	return false;
}
bool			String::operator >=	(String &s) {
	short answ = Compare(s.str);
	if (answ == 1 || answ == 0) return true;
	return false;
}
bool			String::operator <=	(const type *s) {
	short answ = Compare(s);
	if (answ == -1 || answ == 0) return true;
	return false;
}
bool			String::operator <=	(String &s) {
	short answ = Compare(s.str);
	if (answ == -1 || answ == 0) return true;
	return false;
}
bool			String::operator==	(const type * s){
	return !Compare(s);
}
bool			String::operator==	(String & s){
	return !Compare(s);
}
bool			String::operator!=	(const type * s){
	return Compare(s);
}
bool			String::operator!=	(String & s){
	return Compare(s);
}
bool			String::operator!	(){
	return size == 0;
}
				String::operator const type*()const {
	return str;
}

vk::String operator+(const type * str1, vk::String & str2)
{
	return vk::String(str1).Cat(str2);
}
#pragma warning (default: 4996)
VK_NAMESPACE_END