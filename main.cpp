#include "Console.h"
#include "Vector.h"
#include "COWString.h"
#include "RBBST.h"
#include <map>
#include <ctime>

#include <iostream>

namespace s {
	typedef int T;
#define compLT(a,b) (a < b)
#define compEQ(a,b) (a == b)

	typedef enum { BLACK, RED } nodeColor;

	typedef struct Node_ {
		T data;
		nodeColor color;
		struct Node_ *left;
		struct Node_ *right;
		struct Node_ *parent;
	} Node;

#define NIL &sentinel           
	Node sentinel = { 0, BLACK, NIL, NIL, 0};

	Node *root = NIL;

	void rotateLeft(Node *x) {
		Node *y = x->right;

		x->right = y->left;
		if (y->left != NIL) y->left->parent = x;

		if (y != NIL) y->parent = x->parent;
		if (x->parent) {
			if (x == x->parent->left)
				x->parent->left = y;
			else
				x->parent->right = y;
		}
		else {
			root = y;
		}

		y->left = x;
		if (x != NIL) x->parent = y;
	}

	void rotateRight(Node *x) {
		Node *y = x->left;

		x->left = y->right;
		if (y->right != NIL) y->right->parent = x;

		if (y != NIL) y->parent = x->parent;
		if (x->parent) {
			if (x == x->parent->right)
				x->parent->right = y;
			else
				x->parent->left = y;
		}
		else {
			root = y;
		}

		y->right = x;
		if (x != NIL) x->parent = y;
	}

	void insertFixup(Node *x) {
		while (x != root && x->parent->color == RED) {
			if (x->parent == x->parent->parent->left) {
				Node *y = x->parent->parent->right;
				if (y->color == RED) {

					x->parent->color = BLACK;
					y->color = BLACK;
					x->parent->parent->color = RED;
					x = x->parent->parent;
				}
				else {

					if (x == x->parent->right) {
						x = x->parent;
						rotateLeft(x);
					}

					x->parent->color = BLACK;
					x->parent->parent->color = RED;
					rotateRight(x->parent->parent);
				}
			}
			else if (x->parent == x->parent->parent->right) {
				Node *y = x->parent->parent->left;
				if (y->color == RED) {

					x->parent->color = BLACK;
					y->color = BLACK;
					x->parent->parent->color = RED;
					x = x->parent->parent;
				}
				else {

					if (x == x->parent->left) {
						x = x->parent;
						rotateRight(x);
					}
					x->parent->color = BLACK;
					x->parent->parent->color = RED;
					rotateLeft(x->parent->parent);
				}
			}
		}
		root->color = BLACK;
	}
	Node *insertNode(T data) {
		Node *current, *parent, *x;

		current = root;
		parent = 0;
		while (current != NIL) {
			if (data == current->data) return (current);
			parent = current;
			current = (data < current->data) ? current->left : current->right;
		}

		x = reinterpret_cast<Node*>(malloc(sizeof(*x)));
		x->data = data;
		x->parent = parent;
		x->left = NIL;
		x->right = NIL;
		x->color = RED;

		if (parent) {
			if (compLT(data, parent->data))
				parent->left = x;
			else
				parent->right = x;
		}
		else {
			root = x;
		}

		insertFixup(x);
		return(x);
	}

	void deleteFixup(Node *x) {
		while (x != root && x->color == BLACK) {
			if (x == x->parent->left) {
				Node *w = x->parent->right;
				if (w->color == RED) {
					w->color = BLACK;
					x->parent->color = RED;
					rotateLeft(x->parent);
					w = x->parent->right;
				}
				if (w->left->color == BLACK && w->right->color == BLACK) {
					w->color = RED;
					x = x->parent;
				}
				else {
					if (w->right->color == BLACK) {
						w->left->color = BLACK;
						w->color = RED;
						rotateRight(w);
						w = x->parent->right;
					}
					w->color = x->parent->color;
					x->parent->color = BLACK;
					w->right->color = BLACK;
					rotateLeft(x->parent);
					x = root;
				}
			}
			else {
				Node *w = x->parent->left;
				if (w->color == RED) {
					w->color = BLACK;
					x->parent->color = RED;
					rotateRight(x->parent);
					w = x->parent->left;
				}
				if (w->right->color == BLACK && w->left->color == BLACK) {
					w->color = RED;
					x = x->parent;
				}
				else {
					if (w->left->color == BLACK) {
						w->right->color = BLACK;
						w->color = RED;
						rotateLeft(w);
						w = x->parent->left;
					}
					w->color = x->parent->color;
					x->parent->color = BLACK;
					w->left->color = BLACK;
					rotateRight(x->parent);
					x = root;
				}
			}
		}
		x->color = BLACK;
	}
	void deleteNode(Node *z) {
		Node *x, *y;

		if (!z || z == NIL) return;


		if (z->left == NIL || z->right == NIL) {
			y = z;
		}
		else {
			y = z->right;
			while (y->left != NIL)
				y = y->left;
		}

		if (y->left != NIL)
			x = y->left;
		else
			x = y->right;

		x->parent = y->parent;
		if (y->parent)
			if (y == y->parent->left)
				y->parent->left = x;
			else
				y->parent->right = x;
		else
			root = x;

		if (y != z) z->data = y->data;


		if (y->color == BLACK)
			deleteFixup(x);

		free(y);
	}
	Node *findNode(T data) {
		Node *current = root;
		while (current != NIL)
			if (compEQ(data, current->data))
				return (current);
			else
				current = compLT(data, current->data) ?
				current->left : current->right;
		return(0);
	}
}

using namespace vk;
using Color = vk::_Console::ConsoleColor;
int main() {
	COWString str = "asd";

	COWString str1 = str;
	
	COWString str2 = str1 + str;

	/*
	RBBST<int, int> bst;
	
	while (1) {
		int t = 0;
		Console.Read(t);
		system("cls");
		bst.Insert(t, 0);
		bst.Show();

	}

	bst.Delete(1);
	bst.Show();
	*/
/*while (true) {
		Console.SetCursorPositon(0, 0);
		int tmp = 0;
		Console.ReadLine(tmp);
		bst.Insert(tmp, 0);
		bst.Show();
}*/
/*
	RBBST<int, int> bst;
	std::map<int, int> s;
	int b, e;

	//REALESE - x64
	//1'000'000 element
	//173ms for --Insert--
	unsigned int amo = 1'000'000;
	b = clock();
	for (unsigned int i = 0; i <= amo; i++)
		bst.Insert(i ,0);
	e = clock();
	Console.WriteLine(e - b);
	b = clock();
	RBBST<int, int>::Iterator it = bst.Find(amo);
	e = clock();
	Console.WriteLine(e - b);
	Console.Write(*it.Key());
	Console.Write("  ");
	Console.Write(*it.Value());


	Console.WriteLine();
	Console.WriteLine();

	//REALESE - x64
	//1'000'000 element
	//205ms for --Insert--
	b = clock();
	for (unsigned int i = 0; i <= amo; i++)
		s.insert(std::pair<int,int>(i,0));
	e = clock();
	Console.WriteLine(e - b);
	b = clock();
	std::map<int, int>::iterator it1 = s.find(amo);
	e = clock();
	Console.WriteLine(e - b);
	Console.Write(it1->second);
	Console.Write("  ");
	Console.Write(it1->first);
*/
	return 1;
}