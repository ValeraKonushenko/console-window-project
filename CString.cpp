#include "CString.h"

VK_NAMESPACE_BEG
/*~~~~~~~~~~~~~~CHAR~~~~~~~~~~~~~~*/
unsigned int StrLen(const char *str) {
	int i = 0;
	for (i = 0; str[i] != 0; i++);
	return i;
}
short StrCmp(const char * str1, const char * str2){
	while	(*str1 != 0 && *str2 != 0 && 
			((*str1 >= 65 && *str1 <= 90)? *str1 + 32 : *str1) == 
			((*str2 >= 65 && *str2 <= 90) ? *str2 + 32 : *str2)) {
		++str1;
		++str2;
	}
	if (*str1 > *str2)return 1;
	if (*str1 < *str2)return -1;
	return 0;
}
void StrCpy(char * str1, const char * str2) {
	int i;
	for (i = 0; str2[i]; i++)
		str1[i] = str2[i];
	str1[i] = 0;
}



/*~~~~~~~~~~~~~~SHORT~~~~~~~~~~~~~~*/
short StrCmp(const short * str1, const short * str2){
	while	(*str1 != 0 && *str2 != 0 &&
			((*str1 >= 65 && *str1 <= 90) ? *str1 + 32 : *str1) ==
			((*str2 >= 65 && *str2 <= 90) ? *str2 + 32 : *str2)) {
		++str1;
		++str2;
	}
	if (*str1 > *str2)return 1;
	if (*str1 < *str2)return -1;
	return 0;
}
unsigned int StrLen(const short *str){
	int i = 0;
	for (i = 0; str[i] != 0; i++)
		i++;
	return i;
}
void StrCpy(short * str1, const short * str2){
	int i;
	for (i = 0; str2[i]; i++)
		str1[i] = str2[i];
	str1[i] = 0;
}
VK_NAMESPACE_END