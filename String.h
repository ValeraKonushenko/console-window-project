#pragma once
#include "Resourses.h"
#include "Exception.h"
#include "CString.h"
#ifndef VK_NAMESPACE
#define VK_NAMESPACE
#define VK_NAMESPACE_BEG namespace vk{
#define VK_NAMESPACE_END }
#endif

VK_NAMESPACE_BEG
class String{
public:
	using type =	char;
					String			(const type *s = 0);
					String			(String &s);
					~String			();
	uint	Length			() const;
	uint	Capacity		();
	String &		Clear			();
	//Overlaying new data on the old data
	String &		Copy			(const type *s, uint pos = 0, uint amo = 0);
	String &		Copy			(const String & s, uint pos = 0, uint amo = 0);
	String &		Cat				(const type *s, uint amo = 0);
	String &		Cat				(const String & s, uint amo = 0);
	String &		Cat				(const type ch);
	const type *	c_str			();
	bool			IsEmpty			();
	String&			Erase			(uint pos, uint amo);
	String			SubStr			(uint pos, uint amo);
	int				Find			(const type *s)const;
	int				FindFirstOf		(char t) {
		for (uint i = 0; i < size; i++)
			if (str[i] == t)return i;
		return -1;
	}
	int				FindLastOf		(char t) {
		for (int i = size-1; i >= 0; i--)
			if (str[i] == t)return i;
		return -1;
	}
	int				FindFirstNotOf	(char t) {
		for (uint i = 0; i < size; i++)
			if (str[i] != t)return i;
		return -1;
	}
	int				FindLastNotOf	(char t) {
		for (int i = size - 1; i >= 0; i--)
			if (str[i] != t)return i;
		return -1;
	}
	String &		Insert			(uint pos, const type *s);
	short			Compare			(String &s);
	short			Compare			(const type *s);
	//Coping old to new arr(all data are save), and expanding to new capacity/size
	String &		Resize			(uint sz);
	//Deleting all data, and expanding to new capacity/size
	String &		Reserve			(uint new_cap);
	type &			operator []		(uint i);
	String &		operator =		(const type *s);
	String &		operator =		(String &s);
	String &		operator +=		(const type *s);
	String &		operator +=		(String &s);
	String &		operator +=		(type s);
	String			operator +		(const type *s);
	String			operator +		(String &s);
	bool			operator >		(const type *s);
	bool			operator >		(String &s);
	bool			operator <		(const type *s);
	bool			operator <		(String &s);
	bool			operator >=		(const type *s);
	bool			operator >=		(String &s);
	bool			operator <=		(const type *s);
	bool			operator <=		(String &s);
	bool			operator ==		(const type *s);
	bool			operator ==		(String &s);
	bool			operator !=		(const type *s);
	bool			operator !=		(String &s);
	bool			operator !		();

					operator const type *()const;
	friend String operator + (const type * str1, String & str2);
protected:
	type			*str;
	uint	size;
	uint	capacity;
};

VK_NAMESPACE_END