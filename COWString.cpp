#include "COWString.h"

VK_NAMESPACE_BEG
using type = char;
COWString::COWString					(const type * s){
	is_copy = false;
	size = capacity = 0;
	str = nullptr;
	Copy(s);
}
COWString::COWString					(COWString & s){
	*this = s;
}
COWString::~COWString					(){
	if (!is_copy)
		delete[]str;
}
COWString &		COWString::operator=	(COWString & s)
{
	is_copy = true;
	size = s.size;
	capacity = s.capacity;
	str = s.str;
	return *this;
}
COWString &		COWString::Copy			(const type * s, uint pos, uint amo) {
	if (is_copy)Clone();
	if (s == 0 || s == nullptr)	return *this;

	uint new_len = static_cast<uint>(StrLen(s)) + 1;
	if (pos + new_len > capacity)		Resize(pos + new_len);

	if (amo == 0)amo -= 1;

	uint i;
	for (i = 0; s[i] && i < amo; i++)
		str[i + pos] = s[i];
	str[i + pos] = 0;
	size = pos + new_len - 1;
	return *this;
}
COWString &		COWString::Cat			(const type * s, uint amo) {
	if (is_copy)Clone();
	if (s == 0 || s == nullptr)	return *this;

	uint new_len = (amo == 0) ? StrLen(s) + 1 : amo + 1;
	if (size + new_len > capacity)		Resize(size + new_len);

	if (amo == 0)amo -= 1;
	uint i = 0;
	for (i = 0; i < new_len && i < amo; i++)
		str[i + size] = s[i];
	size += new_len - 1;
	return *this;
}
COWString &		COWString::Cat			(const COWString & s, uint amo) {
	if (is_copy)Clone();
	if (s.str == 0 || s.str == nullptr)	return *this;

	uint new_len = (amo == 0) ? s.Length() + 1 : amo + 1;
	if (size + new_len > capacity)		Resize(size + new_len);

	if (amo == 0)amo -= 1;
	uint i = 0;
	for (i = 0; i < new_len && i < amo; i++)
		str[i + size] = s.str[i];

	size += new_len - 1;
	return *this;
}
COWString &		COWString::Cat			(const type ch) {
	if (is_copy)Clone();
	if (size + 1 >= capacity)Resize(size + 1);
	str[size++] = ch;
	return *this;
}
uint			COWString::Length		()					const{
	return this->size;
}
uint			COWString::Capacity		(){
	return this->capacity;
}
COWString &		COWString::Clear		(){
	if (is_copy)
		str = nullptr;
	else
		delete[]str;
	capacity = size = 0;
	is_copy = false;
	return *this;
}
const type *	COWString::c_str		()					const{
	return str;
}
bool			COWString::IsEmpty		()					const
{
	return !size;
}
COWString &		COWString::Erase		(uint pos, uint amo) {
	if (is_copy)Clone();
	if (pos + amo > size)throw Exception("Violation of right acces to the memory");
	for (uint i = pos + amo; i <= size; i++) {
		str[i - amo] = str[i];
		if (!str[i])size = i - amo;
	}
	return *this;
}
COWString		COWString::SubStr		(uint pos, uint amo)const {
	if (pos + amo >= capacity)throw Exception("Vioaltion of right access to the memory");
	return COWString().Copy(str + pos, 0, amo);
}
int				COWString::Find			(const type * s)	const {
	uint new_size = StrLen(s);
	uint pos = 0;
	int len_counter = 0;
	for (uint i = 0; i < size && len_counter != new_size; i++)
		for (uint j = 0; j < new_size; j++)
			if (str[i + j] == s[j]) {
				pos = i + j;
				len_counter++;
			}
			else {
				len_counter = 0;
				break;
			}

	if (len_counter != 0)
		return pos + 1 - new_size;
	return -1;
}
COWString &		COWString::Insert		(uint pos, const type * s) {
	if (is_copy)Clone();
	uint len = StrLen(s);
	capacity += len;
	type *new_str = new type[capacity];
	uint i = 0, f = 0;
	//copying all before POS
	while (i < pos)
		new_str[i++] = str[f++];

	//copying the insert part
	for (uint l = 0; l < len; l++)
		new_str[i++] = s[l];

	//copying all after pos+len_it
	while (str[f])
		new_str[i++] = str[f++];
	new_str[i] = 0;
	size += len;
	delete[] str;
	str = new_str;
	return *this;
}
int				COWString::FindFirstOf	(char t)			const {
	for (uint i = 0; i < size; i++)
		if (str[i] == t)return i;
	return -1;
}
int				COWString::FindLastOf	(char t)			const {
	for (int i = size - 1; i >= 0; i--)
		if (str[i] == t)return i;
	return -1;
}
int				COWString::FindFirstNotOf(char t)			const {
	for (uint i = 0; i < size; i++)
		if (str[i] != t)return i;
	return -1;
}
int				COWString::FindLastNotOf(char t)			const {
	for (int i = size - 1; i >= 0; i--)
		if (str[i] != t)return i;
	return -1;
}
short			COWString::Compare		(const COWString & s)const {
	return static_cast<short>(StrCmp(str, s.str));
}
short			COWString::Compare		(const type * s)	const {
	return static_cast<short>(StrCmp(str, s));
}
COWString &		COWString::Resize		(uint sz) {
	if (is_copy)Clone();
	type *new_str = new type[sz];
	for (uint i = 0; i <= size && str; i++)
		new_str[i] = str[i];
	delete[] str;
	str = new_str;
	capacity = sz;
	return *this;
}
COWString &		COWString::Reserve		(uint new_cap) {
	if (is_copy)Clone();
	type *new_str = new type[new_cap];
	Clear();
	str = new_str;
	capacity = new_cap;
	return *this;
}
type &			COWString::operator[](uint i) {
	if (is_copy)Clone();
	if (i >= capacity) throw Exception("Violation of right access to the memory");
	return str[i];
}
COWString &		COWString::operator=	(const type * s) {
	Clear();
	Copy(s);
	return *this;
}
COWString &		COWString::operator+=	(const type * s) {
	if (is_copy)Clone();
	Cat(s);
	return *this;
}
COWString &		COWString::operator+=	(const COWString & s) {
	if (is_copy)Clone();
	Cat(s);
	return *this;
}
COWString &		COWString::operator+=	(const type s) {
	if (is_copy)Clone();
	Cat(s);
	return *this;
}
COWString		COWString::operator+	(const type * s)	const {
	return COWString(str).Cat(s);
}
COWString		COWString::operator+	(const COWString & s)const {
	return COWString(str).Cat(s);
}
bool			COWString::operator >	(const type *s)		const {
	if (Compare(s) == 1) return true;
	return false;
}
bool			COWString::operator >	(const COWString &s)const {
	if (Compare(s.str) == 1) return true;
	return false;
}
bool			COWString::operator <	(const type *s)		const {
	if (Compare(s) == -1) return true;
	return false;
}
bool			COWString::operator <	(const COWString &s)const {
	if (Compare(s.str) == -1) return true;
	return false;
}
bool			COWString::operator >=	(const type *s)		const {
	short answ = Compare(s);
	if (answ == 1 || answ == 0) return true;
	return false;
}
bool			COWString::operator >=	(const COWString &s)const {
	short answ = Compare(s.str);
	if (answ == 1 || answ == 0) return true;
	return false;
}
bool			COWString::operator <=	(const type *s)		const {
	short answ = Compare(s);
	if (answ == -1 || answ == 0) return true;
	return false;
}
bool			COWString::operator <=	(const COWString &s)const {
	short answ = Compare(s.str);
	if (answ == -1 || answ == 0) return true;
	return false;
}
bool			COWString::operator==	(const type * s)	const {
	return !Compare(s);
}
bool			COWString::operator==	(const COWString & s)const {
	return !Compare(s);
}
bool			COWString::operator!=	(const type * s)	const {
	return Compare(s);
}
bool			COWString::operator!=	(const COWString & s)const {
	return Compare(s);
}
bool			COWString::operator!	() {
	return size == 0;
}
COWString::operator const type*() const{
	return str;
}
void COWString::Clone(){
	type *fake_str = new type[capacity];
	for (uint i = 0; i < capacity; i++)
		fake_str[i] = str[i];
	str = fake_str;
	is_copy = false;
}

VK_NAMESPACE_END
